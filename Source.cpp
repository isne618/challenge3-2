#include<iostream>
#include<cstdlib>
#include<ctime>
#include<algorithm>
#include<vector>
#include "customer.cpp"
using namespace std;

void bubblesort(vector<int>x)
{
	bool swap = true;
	while (swap)
	{
		swap = false;
		for (int i = 0; i < x.size()-1; i++)
		{
			if (x[i] > x[i + 1])
			{
				x[i] += x[i + 1];
				x[i + 1] = x[i] - x[i + 1];
				x[i] -= x[i + 1];
				swap = true;
			}
		}
	}
}

void main()
{
	vector<int>arrive_time;
	vector<int>service_time;
	vector<int>wait_time;
	vector<int>left_time;
	vector<int>cashier;
	int max_cus, max_ser, min_cus, min_ser, num_cash, num_cus, i, arrive, service, sum = 0;
	cout << "Input maximum customer: ";
	cin >> max_cus;
	cout << "Input minimum customer: ";
	cin >> min_cus;
	cout << "Input maximum service time: ";
	cin >> max_ser;
	cout << "Input minimum service time: ";
	cin >> min_ser;
	cout << "Input number of cashier: ";
	cin >> num_cash;
	//random number of customer
	srand(time(0));
	num_cus = rand() % (max_cus - min_cus) + min_cus; 
	//random arrived time
	for (i = 0; i < num_cus; i++) 
	{
		arrive = rand() % 240; //random arrived time in 4 hr (240 mins)
		arrive_time.push_back(arrive);
	}
	sort(arrive_time.begin(), arrive_time.end());

	//random service time
	for (i = 0; i < num_cus; i++)
	{
		service = rand() % (max_ser - min_ser) + min_ser;
		service_time.push_back(service);
	}

	i = 0;
	while (i < num_cus)
	{
		while (i < num_cash) 
		{
			wait_time.push_back(0); //waiting time for the first customer
			sum += wait_time.at(i); //sum of waiting time
			left_time.push_back(arrive_time.at(i) + service_time.at(i)); //left time
			cashier.push_back(left_time.at(i));
			bubblesort(cashier);
			i++; //next customer
		}
		if (cashier.front() - arrive_time.at(i) < 0) //if waiting time less than 0 mean customer no need to wait
		{
			wait_time.push_back(0); //no need to wait = wait time is 0
			left_time.push_back(arrive_time.at(i) + service_time.at(i)); //left time
		}
		else //customer need to wait
		{
			wait_time.push_back(cashier.front() - arrive_time.at(i));
			left_time.push_back(arrive_time.at(i) + service_time.at(i)); //left time
		}
		cashier.push_back(left_time.at(i));
		bubblesort(cashier);
		sum += wait_time.at(i);
		i++;
	}
	//show the queue
	cout << "--------------------------------" << endl;
	for (i = 0; i < num_cus; i++)
	{
		cout << "Customer " << i + 1 << " --arrived " << arrive_time.at(i) << " --wait time " << wait_time.at(i) << " --left " << left_time.at(i);
		cout << endl;
	}
	//average
	cout << "--------------------------------" << endl;
	cout << "Average wait time: " << sum / num_cus << " minutes.";
}